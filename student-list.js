"use strict";
exports.__esModule = true;
exports.Student_List = void 0;
var Student_List = /** @class */ (function () {
    function Student_List() {
        var _this = this;
        this.StudentName = new Array(9);
        this.StudentList = ["Somchai", "Sompong",
            "Somsi", "Prayut",
            "Prawit", "Pareena",
            "John", "Bob",
            "Jane", "Noi"
        ];
        this.setName = function (StudentList) {
            StudentList.forEach(function (element) {
                _this.StudentName.push(element);
            });
        };
        this.setName(this.StudentList);
    }
    return Student_List;
}());
exports.Student_List = Student_List;
